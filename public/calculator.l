/* lexical grammar */
%{
var reserved_words ={ PI: 'PI', E : 'E', PROCEDURE: 'procedure', VAR: 'var', END: 'end', CONST: 'const', IF: 'if', THEN: 'then', ELSE: 'else', BEGIN: 'begin', CALL: 'call', ODD: 'odd', P: 'p', WHILE: 'while', DO: 'do'}

function idORrw(x) {
  return (x.toUpperCase() in reserved_words)? x.toUpperCase() : 'ID'
}

%}
%%

\s+|\#.*                         /* skip whitespace and comments */
\b\d+("."\d*)?([eE][-+]?\d+)?\b  return 'NUMBER'
\b[A-Za-z_]\w*\b                 return idORrw(yytext)
[<>=!][=]|[<>]                   return 'COMPARISION'
[-*/+^!%=();]                    return yytext;
<<EOF>>                          return 'EOF'
[(]                              return 'LEFTPAR'
")"                              return 'RIGHTPAR'
":"                              return 'COLON' 
";"                              return 'SEMICOLON'
"."                              return 'DOT'
","                              return 'COMMA'



