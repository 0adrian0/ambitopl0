/* description: Parses end executes mathematical expressions. */

%{

var idFatherProcedure = [];       /* Lista que va almacenando y eliminando el identificador numerico de cada 
                                 proceso. Me permite saber en cada momento el padre del proceso actual*/
var procedures_sym_table = [];   /* Array de procesos con sus declaraciones */
var actualProcess;               /* Variable que indica la posicion en el array de procedures_sym_table del
                                 proceso actual  */
var declarated; /* Variable que nos sirve para saber si un identificador fue declarado o no */
var IdIsConstant; /* Variable que nos sirve para saber si un identificador es una constante o no */

var finded; /* Variable que nos sirve para saber cuando se ha encontrado un proceso y cuando no */
  
var symbol_table = {};


var addProcess = function(processName) {
  if (processName == "block") {
    procedures_sym_table = procedures_sym_table.concat({name: processName, fatherProcess: "null", numberParams: 0, declarations: []});
    idFatherProcedure = idFatherProcedure.concat(0);
    actualProcess = 0;
    //alert("actualProcess = " + actualProcess);
  } else {
    var father;
    if (idFatherProcedure.length == 1) {
      father = idFatherProcedure[0]; 
    } else { 
      father = idFatherProcedure[idFatherProcedure.length - 1];
    }
    actualProcess = procedures_sym_table.length;
    procedures_sym_table = procedures_sym_table.concat({name: processName, fatherProcess: father, numberParams: 0, declarations: []}); 
    
    //alert("actualProcess = " + actualProcess + " padre: " + father);
  }
}
  
var identificatorDeclarated = function(identificatorName) {
  declarated = false;
  var idProcess = actualProcess;
  //alert("idProcess = " + idProcess);
  while (idProcess != "null") {
    
    for(var i = 0; i < procedures_sym_table[idProcess].declarations.length; i++){
      if (procedures_sym_table[idProcess].declarations[i].type == "identificator") {
        if (procedures_sym_table[idProcess].declarations[i].name == identificatorName) {
          declarated = true;
        }
      }
    }
    idProcess = procedures_sym_table[idProcess].fatherProcess;
    //alert("idProcess = " + idProcess);
  }
}


var identificatorIsConstant = function(identificatorName) {
  IdIsConstant = false;
  var idProcess = actualProcess;
  //alert("idProcess = " + idProcess);
  while (idProcess != "null") {
    for(var i = 0; i < procedures_sym_table[idProcess].declarations.length; i++){
      if (procedures_sym_table[idProcess].declarations[i].type == "constant") {
        if (procedures_sym_table[idProcess].declarations[i].name == identificatorName) {
          IdIsConstant = true;
        }
      }
    }
    idProcess = procedures_sym_table[idProcess].fatherProcess;
    //alert("idProcess = " + idProcess);
  }
}

var findProcess = function(processName) {
  finded = false;
  for(var i = 0; i < procedures_sym_table.length; i++){
    if (procedures_sym_table[i].name == processName) {
      finded = true;
      actualProcess = i;
    }
  }
}

var SetNumParamProcess = function(processName, numParam) {
  for(var i = 0; i < procedures_sym_table.length; i++){
    if (procedures_sym_table[i].name == processName) {
      procedures_sym_table[i].numberParams = numParam;
    }
  }
}

%}

%token NUMBER COMPARISION ID E PI EOF PROCEDURE LEFTPAR RIGHTPAR COLON SEMICOLON
/* operator associations and precedence */

%right '='
%left '+' '-'
%left '*' '/'
%left '^'
%right '%'
%left UMINUS
%left '!'

%start prog

%% /* language grammar */

/* ------------------------ */


prog
    : fatherProcess constsvars block DOT EOF
        { 
          $$ = $3; 
          return $$;
        }
    ;
    
fatherProcess
    : /* empty */ 
        { 
           procedures_sym_table = [];
           idFatherProcedure = [];
           addProcess("block");
        }
    ;

constsvars
    : consts vars
      {  
         if (($1) && ($2)) $$ = $1.concat($2); 
         else if ($1) $$ = $1;
         else if ($2) $$ = $2;
      }
    ;

consts
    : CONST constants ';' consts
        { 
          if ($4) $$ = $2.concat($4); 
          else $$ = $2;
        }
    | /* empty */
    ;

vars
    : VAR identificators ';' vars
        { 
          if ($4) $$ = $2.concat($4); 
          else $$ = $2;
        }
    | /* empty */
    ; 

block 
    : process statements
        { 
          $$ = {procedures: $1, statements: $2}; 
        }
    | statements
        { 
          $$ = $1; 
        }
    ;


process
    : PROCEDURE idproc '(' variables ')' COLON constsvars block END ';'
      { 
        SetNumParamProcess($2, $4.length);
        idFatherProcedure.pop();
        actualProcess = actualProcess - 1;
        $$ = [ "process", {name: $2}, {parameters: $4}, {content: $8}]; 
      }
    | PROCEDURE idproc COLON constsvars block END ';'
      { 
        idFatherProcedure.pop();
        actualProcess = actualProcess - 1;
        $$ = [ "process", {name: $2}, {content: $5}]; 
      }
    | process PROCEDURE idproc '(' variables ')' COLON constsvars block END ';'
      { 
        SetNumParamProcess($3, $5.length);
        idFatherProcedure.pop();
        actualProcess = actualProcess - 1;
        if ($3) $$ = $1.concat(["process", {name: $3}, {parameters: $5}, {content: $9}]); 
        else $$ = $1;
      }
    | process PROCEDURE idproc COLON constsvars block END ';'
      { 
        idFatherProcedure.pop();
        actualProcess = actualProcess - 1;
        if ($3) $$ = $1.concat(["process", {name: $3}, {content: $6}]); 
        else $$ = $1;
      }
    ;

idproc
    : ID
      { 
        addProcess($1);
        idFatherProcedure = idFatherProcedure.concat(actualProcess);
      }
    ;

variables
    : ID
      { $$ =  (typeof $1 === 'undefined')? [] : [{name: $1}]; }
    | variables COMMA ID
      { 
         if ($3) $$ = $1.concat({name: $3}); 
         else $$ = $1;
      }
    ;

constants 
    : ID '=' NUMBER
      { 
        procedures_sym_table[actualProcess].declarations = procedures_sym_table[actualProcess].declarations.concat({name: $1, type: "constant", value: $3});
        $$ =  (typeof $1 === 'undefined')? [] : [{name: $1, value: $3}]; 
        
      }
    | constants COMMA ID '=' NUMBER
      {  
          procedures_sym_table[actualProcess].declarations = procedures_sym_table[actualProcess].declarations.concat({name: $3, type: "constant", value: $5});
         if ($3) $$ = $1.concat({name: $3, value: $5}); 
         else $$ = $1;
      }
    ;

identificators
    : ID
      { 
        procedures_sym_table[actualProcess].declarations = procedures_sym_table[actualProcess].declarations.concat({name: $1, type: "identificator"});
        
        $$ =  (typeof $1 === 'undefined')? [] : [{name: $1}];
      }
    | identificators COMMA ID
      { 
         procedures_sym_table[actualProcess].declarations = procedures_sym_table[actualProcess].declarations.concat({name: $3, type: "identificator"});
          
         if ($3) $$ = $1.concat({name: $3}); 
         else $$ = $1;
      }
    ;

statements
    : /* empty */
    | statement statements 
      {  
         if ($2) $$ = $1.concat($2); 
         else $$ = $1;
      }
    ;

statement
    : ID '=' expression 
      { 
        identificatorDeclarated($1);
        identificatorIsConstant($1);  
        if (IdIsConstant == true) throw new Error("El identificador '" + $1 + "' es una constante y no puede modificarse. ");
        else if (declarated == false) throw new Error("El identificador '" + $1 + "' no ha sido declarado. ");
        $$ = ["ASSIGN", ["left:", {ID: $1}, "right:", $3]];
       }
    | ID '=' expression ';'
      { 
        identificatorDeclarated($1);  
        identificatorIsConstant($1);
        if (IdIsConstant == true) throw new Error("El identificador '" + $1 + "' es una constante y no puede modificarse. ");
        else if (declarated == false) throw new Error("El identificador '" + $1 + "' no ha sido declarado. ");
        $$ = ["ASSIGN", ["left:", {ID: $1}, "right:", $3]];
      }
    | IF condition THEN statements ELSE statements END ';'
       { $$ = ["If-Then-Else", [{condition: $2 }, "left:", $4, "right", $6]];}
    | IF condition THEN statements END ';'
       { $$ = ["If-Then", [{condition: $2 }, "statements:", $4]];}
    | CALL ID '(' variables ')' ';' 
      {  
        var aux = actualProcess;
        findProcess($2);
        if (finded == false) {
          throw new Error("Error en CALL " + $2 + ". Ese proceso no ha sido declarado. ");
        } else if (procedures_sym_table[actualProcess].numberParams != $4.length) {
          throw new Error("Error en CALL " + $2 + ". Ese proceso recibe un número diferente de argumentos. ");
        } else {
          actualProcess = aux;
          $$ = ["CALL", [{procedure: $2, arguments: $4}]];
        }
      } 
    | CALL ID ';' 
      {  
        var aux = actualProcess;
        findProcess($2);
        if (finded == false) {
          throw new Error("Error en CALL " + $2 + ". Ese proceso no ha sido declarado. ");
        } else if (procedures_sym_table[actualProcess].numberParams != 0) {
          throw new Error("Error en CALL " + $2 + ". Ese proceso recibe un número diferente de argumentos. ");
        } else {
          actualProcess = aux;
          $$ = ["CALL", [{procedure: $2}]];
        }
        
      } 
    | P expression ';'
      {  $$ = ["P", [{expresion: $2}]];}
    | WHILE condition DO statements END ';'
      {  $$ = ["WHILE", [{condition: $2 }, "statements:", $4]]; }
    | BEGIN statements END ';'
      {  $$ = ["BEGIN-END", ["statements:", $2]]; }
    ;


condition
    : ODD expression
      { $$ = [{odd: $2 }];}
    | expression COMPARISION expression
      { $$ = [{comparision: $2 }, "left", $1, "right", $3];}
    ;


expression
    : expression '+' expression
        {$$ = ["+", ["left:", $1, "right:", $3]]; }
    | expression '-' expression
        {$$ = ["-", ["left:", $1, "right:", $3]];}
    | expression '*' expression
        {$$ = ["*", ["left:", $1, "right:", $3]];}
    | expression '/' expression
        {
          if ($3 == 0) throw new Error("Division by zero, error!");
          $$ = ["/", ["left:", $1, "right:", $3]];
        }
    | expression '^' expression
        {$$ = ["^", ["left:", $1, "right:", $3]];}
    | expression '!'
        {
          if ($1.number % 1 !== 0) 
             throw "Error! Attempt to compute the factorial of "+
                   "a floating point number "+$1;
          $$ = ["!", $1];
        }
    | expression '%'
        {$$ = ["%", $1];}
    | '-' expression %prec UMINUS
        {$$ = -$2;}
    | '(' expression ')'
        {$$ = $2;}
    | NUMBER
        {
          $$ = {number: Number(yytext)};
        }
    | E
        {$$ = Math.E;}
    | PI
        {$$ = Math.PI;}
    | ID 
        {$$ = symbol_table[yytext] || {variable: yytext} }
    ;



